<?php

use Phalcon\Mvc\Controller;
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;

class LoginController extends Controller {
	public function isloggedin($userid) {
		$profile = Basicprofile::findFirstByUserid($userid);
		if(isset($userid)) {
			$this->response->redirect('dashboard');
		}
	}
	public function loginpageAction() {
		$this->tag->setTitle('PlanEat — Inicia Sesión');
		$userid = $this->session->get('_id');
		$this->isloggedin($userid);
	}
}