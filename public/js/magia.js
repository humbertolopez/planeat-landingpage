$(document).ready(function(){

	//lila expert data
	var lila = {
		"id": "1",
		"name": "Lila Flores Manzo",
		"shortname" : "Lila",
		"img": "lila.jpg",
		"desc": [
			"Licenciatura en nutrición y ciencia de los alimentos",
			"“Para mí, la nutrición es alimentarte de manera consciente y saludable porque el cuerpo es el vehículo con el que realizas tus sueños, metas y aspiraciones”.",
		],
		"expertise" : [
			"Control de peso",
			"Patologías",
			"Diabetes",
			"Conocimiento sobre nutrición deportiva"
		]
	};

	var lilaplans = {
		"title" : "Planes Lila",
		"planid" : "clicklila",
		"includes" : [
			"Plan de alimentación mensual",
			"Consejos y recomendaciones",
			"2 preguntas al mes",
			"Mensajes de motivación y seguimiento a través de WhatsApp",
			"Seguimiento a través de PlanEat App"
		],
		"plans" : [
			{
				"price" : "199.00",
				"term" : "Un solo mes",
				"recurring" : true,
				"smalltext" : "Un solo cargo. Precio más IVA.",
				"action": "lilasingle"
			}
		]
	};

	var lab = 	{
		"id": "2",
		"name": "Fitness and Nutrition Lab",
		"shortname" : "Fitness",
		"img": "lab.jpg",
		"desc": [
			"Profesionales en nutrición deportiva y especialistas en alto rendimiento",
			"“El propósito de nuestro Laboratorio es brindar un soporte técnico y cientifico, para todos aquellos que se esfuerzan por mejorar su desempeño, su salud y binestar. Donde ofrecemos servicios de evaluaciones fisicas y de nutrición”."
		],
		"expertise": [
			"Evaluaciones nutricionales",
			"Evaluación física",
			"Programas de nutrición",
			"Programas de entrenamiento físico",
			"Programas de nutrición con entrenamiento físico"
		]
	};

	var labplans = {
		"title" : "Planes Fitness and Nutrition Lab",
		"planid" : "clicklab",
		"includes" : [
			"Plan de alimentación mensual",
			"Consejos y recomendaciones",
			"2 preguntas al mes",
			"Mensajes de motivación y seguimiento a través de WhatsApp",
			"Seguimiento a través de PlanEat App"
		],
		"plans" : [
			{
				"price" : "199.00",
				"term" : "Básico",
				"smalltext" : "Un solo cargo. Precio más IVA.",
				"recurring" : false,
				"action": "labsingle"
			}
		]
	};

	var sofia = {
		"id" : "3",
		"name" : "Sofía Jiménez Bojorques",
		"shortname" : "Sofía",
		"img" : "sofia.jpg",
		"desc" : [
			"Licenciatura Nutrición y ciencia de los alimentos. Nutricíon vegetariana y vegana.",
			"“En nuestros platos comienza la decision de obtener bienestar; a traves de un adecuado balance de alimentos podemos transformar nuestra salud de manera deliciosa y accesible.”"
		],
		"expertise" : [
			"Control de peso",
			"Patologías (alergias, intolerancias, etc)",
			"Alimentación vegana y vegetariana",
			"Conocimiento de nutrición deportiva"
		]
	};

	var sofiaplans = {
		"title" : "Planes Sofía",
		"planid" : "clicksofia",
		"includes" : [
			"Plan de alimentación mensual",
			"Consejos y recomendaciones",
			"2 preguntas al mes",
			"Mensajes de motivación y seguimiento a través de WhatsApp",
			"Seguimiento a través de PlanEat App"
		],
		"plans" : [
			{
				"price" : "199.00",
				"term" : "Básico",
				"recurring" : false,
				"smalltext" : "Un solo cargo. Precio más IVA.",
				"action": "sofiasingle"
			}
		]
	};

	var cristina = {
		"id" : "4",
		"name" : "Cristina Colin",
		"shortname" : "Cristina",
		"img" : "cristina.jpg",
		"desc" : [
			"Licenciatura en nutrición. Coach Alimenticio, Health coach por Institute for Integrative Nutrition",
			"El concepto de salud engloba un estado de bienestar en todas las áreas de la vida. Mi enfoque es crear un equilibrio físico, mental y social que lleve a mis pacientes a lograr sus objetivos."
		]
	};

	var cristinaplans = {
		"title" : "Planes Cristina",
		"planid" : "clickcristina",
		"includes" : [
			"Plan de alimentación mensual",
			"Consejos y recomendaciones",
			"2 preguntas al mes",
			"Mensajes de motivación y seguimiento a través de WhatsApp",
			"Seguimiento a través de PlanEat App"
		],
		"plans" : [
			{
				"price" : "199.00",
				"term" : "Básico",
				"recurring" : false,
				"smalltext" : "Un solo cargo. Precio más IVA.",
				"action": "cristinasingle"
			}
		]
	};

	var german = {
		"id" : "5",
		"name" : "Germán Álvarez — Station Fitness 24",
		"shortname" : "Germán",
		"img" : "german.jpg",
		"desc" : [
			"Licenciatura en nutrición",
			"“Dieta no significa martirio, sino una manera de alimentarse adecuadamente para mejorar su desempeño, su salud, bienestar y así lograr metas”"
		],
		"expertise" : [
			"Evaluación nutricional",
			"Nutrición deportiva",
			"Control de peso"
		]
	};

	var germanplans = {
		"title" : "Planes Germán",
		"planid" : "clickgerman",
		"includes" : [
			"Plan de alimentación mensual",
			"Consejos y recomendaciones",
			"2 preguntas al mes",
			"Mensajes de motivación y seguimiento a través de WhatsApp",
			"Seguimiento a través de PlanEat App"
		],
		"plans" : [
			{
				"price" : "199.00",
				"term" : "Básico",
				"recurring" : false,
				"smalltext" : "Un solo cargo. Precio más IVA.",
				"action": "germansingle"
			}
		]
	};

	var dennis = {
		"id" : "6",
		"name" : "Dennis Camacho",
		"shortname" : "Dennis",
		"img" : "dennis.jpg",
		"desc" : [
			"“Mi nombre es Dennis Camacho y soy nutriologa, mamá, deportista y quiero apoyarte a lograr tus objetivos, juntos haremos cambios para que goces de una vida saludable,  haremos un plan alimenticio inteligente”"
		]
	};

	var dennisplans = {
		"title" : "Planes Dennis",
		"planid" : "clickdennis",
		"includes" : [
			"Plan de alimentación mensual",
			"Consejos y recomendaciones",
			"2 preguntas al mes",
			"Mensajes de motivación y seguimiento a través de WhatsApp",
			"Seguimiento a través de PlanEat App"
		],
		"plans" : [
			{
				"price" : "199.00",
				"term" : "Básico",
				"recurring" : false,
				"smalltext" : "Un solo cargo. Precio más IVA.",
				"action": "dennissingle"
			}
		]
	};

	var michelle = {
		"id" : "7",
		"name" : "Michelle Robles — Station 24 Fitness",
		"shortname" : "Michelle",
		"img" : "michelle.jpg",
		"desc" : [
			"“El hombre sabio debería de considerar que la salud es la mayor de las bendiciones humanas”. Agradece a tu cuerpo lo que hace por ti."
		],
		"expertise" : [
			"Educación nutricional",
			"Reducción de sobrepeso y obesidad",
			"Aumento de peso",
			"Enfermedades crónico-degenerativas"
		]
	};

	var michelleplans = {
		"title" : "Planes Michelle",
		"planid" : "clickmichelle",
		"includes" : [
			"Plan de alimentación mensual",
			"Consejos y recomendaciones",
			"2 preguntas al mes",
			"Mensajes de motivación y seguimiento a través de WhatsApp",
			"Seguimiento a través de PlanEat App"
		],
		"plans" : [
			{
				"price" : "199.00",
				"term" : "Básico",
				"recurring" : false,
				"smalltext" : "Un solo cargo. Precio más IVA.",
				"action": "michellesingle"
			}
		]
	}

	var weight = [lila,lab,sofia,dennis];
	var conditions = [lila,michelle];
	var sports = [german];
	var vegan = [sofia];
	var all = [lila,lab,sofia,cristina,german,dennis,michelle];
	var allplans = [lilaplans,labplans,sofiaplans,cristinaplans,germanplans,dennisplans,michelleplans];

	var experts = new Vue({
		el: '#experts',
		data: {
			experts: all
		},
		methods: {
			weightnav: function(){
				this.experts = weight;
			},
			sportsnav: function(){
				this.experts = sports;
			},
			conditionsnav: function(){
				this.experts = conditions;
			},
			vegannav: function(){
				this.experts = vegan;
			},
			allnav: function(){
				this.experts = all;
			}
		},
		filters: {
			imgsrc : function(filename){
				return "img/experts/" + filename;
			}
		}
	});

	var plans = new Vue({
		el: '#plans',
		data: {
			plans: allplans
		},
		methods: {
			clicklila: function(){
				this.plans = [lilaplans];
			},
			clicklab: function(){
				this.plans = [labplans];
			},
			clicksofia: function(){
				this.plans = [sofiaplans];
			},
			clickcristina: function(){
				this.plans = [cristinaplans];
			},
			clickgerman: function(){
				this.plans = [germanplans];
			},
			clickdennis: function(){
				this.plans = [dennisplans];
			},
			clickmichelle: function(){
				this.plans = [michelleplans];
			},
			clickall: function(){
				this.plans = allplans;
			}
		},
		filters: {
			href : function(plantype){
				return "plans/" + plantype;
			}
		}
	});

	Vue.directive('carousel',{
		inserted: function(el){
			el.owlCarousel();
		}
	});

	function fadeExperts(){

	}

	$('body').scrollspy({target: '#planeatnav'});

});