<?php

use Phalcon\Mvc\Controller;

class SignController extends Controller {
	public function isloggedin($userid) {
		$profile = Basicprofile::findFirstByUserid($userid);
		if(isset($userid)) {
			$this->response->redirect('dashboard');
		}
	}
	public function signuppageAction() {
		$this->tag->setTitle('PlanEat — Crea tu Cuenta');
		$userid = $this->session->get('_id');
		$this->isloggedin($userid);
	}
}

