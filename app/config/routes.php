<?php
// Create the router
$router = new \Phalcon\Mvc\Router();
// Remove slashes on urls end
$router->removeExtraSlashes(true);
// Login page
$router->add(
    "/login",
    array(
        "controller" => "login",
        "action"     => "loginpage",
    )
);
// Signup page
$router->add(
    "/signup",
    array(
        "controller" => "sign",
        "action"     => "signuppage",
    )
);
$router->add(
    "/buy",
    array(
        "controller" => "buy",
        "action"     => "buypage",
    )
);
$router->add(
    "/letspay",
    array(
        "controller" => "buy",
        "action"     => "letspaypage",
    )
);
$router->add(
    "/dashboard",
    array(
        "controller" => "dash",
        "action"     => "dashboardpage",
    )
);
$router->add(
    "/dashboard/weekly",
    array(
        "controller" => "dash",
        "action"     => "weeklypage",
    )
);
$router->add(
    "/dashboard/habits",
    array(
        "controller" => "dash",
        "action"     => "habitspage",
    )
);
$router->add(
    "/dashboard/activity",
    array(
        "controller" => "dash",
        "action"     => "activitypage",
    )
);
$router->add(
    "/dashboard/measures",
    array(
        "controller" => "dash",
        "action"     => "measurespage",
    )
);
$router->add(
    "/dashboard/sleep",
    array(
        "controller" => "dash",
        "action"     => "sleeppage",
    )
);
$router->add(
    "/dashboard/profile",
    array(
        "controller" => "dash",
        "action"     => "profilepage",
    )
);
$router->add(
    "/dashboard/basic",
    array(
        "controller" => "dash",
        "action"     => "basicspage",
    )
);
//$router->add(
//    "/dashboard/plan",
//    array(
//        "controller" => "dash",
//        "action"     => "planpage",
//    )
//);
// If payment is successfull
$router->add(
    "/dashboard/paymentsuccess",
    array(
        "controller" => "dash",
        "action"     => "paymentsuccesspage",
    )
);
// If payment is denied
$router->add(
    "/dashboard/paymentfail",
    array(
        "controller" => "dash",
        "action"     => "paymentfailpage",
    )
);
// If payment is pending
$router->add(
    "/dashboard/paymentpending",
    array(
        "controller" => "dash",
        "action"     => "paymentpendingpage",
    )
);
// If payment is successfull
$router->add(
    "/dashboard/paymentinprocess",
    array(
        "controller" => "dash",
        "action"     => "paymentinprocesspage",
    )
);
// Notifications page for mercadopago
$router->add(
    "/notifications/mp",
    array(
        "controller" => "notifications",
        "action"     => "mp",
    )
);

$router->handle();