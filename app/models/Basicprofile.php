<?php

use Phalcon\Mvc\Model;
use Phalcon\Db\RawValue;

class Basicprofile extends Model {

	public function beforeValidationOnCreate(){
		if(!$this->timestamp) {
			$this->timestamp = new RawValue('default');
		}
	}

	public $id;
	public $userid;
	public $sex;
	public $age;
	public $weight;
	public $height;
	public $activity;
	public $intensity;
	public $objective;
	public $considerations;
	public $timestamp;
}