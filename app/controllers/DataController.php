<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;

class DataController extends Controller {

	public function sendbasicdataAction(){
		$basicprofile = new Basicprofile();

		$basicprofile->userid = $this->session->get('_id');
		$basicprofile->sex = $this->request->getPost('sexfield');
		$basicprofile->age = $this->request->getPost('agefield');
		$basicprofile->weight = $this->request->getPost('weightfield');
		$basicprofile->height = $this->request->getPost('heightfield');
		$basicprofile->activity = $this->request->getPost('activityfield');
		$basicprofile->intensity = $this->request->getPost('intensityfield');
		$basicprofile->objective = $this->request->getPost('objectivefield');
		$basicprofile->considerations = $this->request->getPost('considerationsfield');

		$existentprofile = Basicprofile::findFirstByUserid($this->session->get('_id'));

		if(!$existentprofile) {

			$basicprofile->save();

			if($basicprofile->save()){
				$this->flashSession->success("¡Perfecto! Ya tenemos tus datos.");
				$this->response->redirect('dashboard/basic');	
			} else {
				$this->flashSession->error("¡Espera! Algo salió mal. Inténtalo nuevamente más tarde.");
				$this->response->redirect('dashboard/basic');	
			}

		} else {
			$existentprofile->update([
					'sex' => $this->request->getPost('sexfield'),
					'age' => $this->request->getPost('agefield'),
					'weight' => $this->request->getPost('weightfield'),
					'height' => $this->request->getPost('heightfield'),
					'activity' => $this->request->getPost('activityfield'),
					'intensity' => $this->request->getPost('intensityfield'),
					'objective' => $this->request->getPost('objectivefield'),
					'considerations' => $this->request->getPost('considerationsfield')
				]);
			if($existentprofile->update()){
				$this->flashSession->success("¡Muy bien! Tus datos se han actualizado.");
				$this->response->redirect('dashboard/basic');
			} else {
				$this->flashSession->error("¡Oops! Algo falló al intentar actualizar tus datos. Por favor, Inténtalo más tarde.");
				$this->response->redirect('dashboard/basic');
			}
	
		}

	}

	public function sendprofiledataAction() {
		$user = new Users();
		$existenuserdata = Users::findFirstById($this->session->get('_id'));

		$userfirstname = $this->request->getPost('namefield');
		$userlastname = $this->request->getPost('lastnamefield');

		$existenuserdata->update([
				'name' => $userfirstname,
				'lastname' => $userlastname
			]);

		if($existenuserdata->update()) {
			$this->flashSession->success("¡Muy bien! Tus datos se han actualizado.");
			$this->response->redirect('dashboard/profile');	
		} else {
			$this->flashSession->error("¡Oops! Algo falló al intentar actualizar tus datos. Por favor, Inténtalo más tarde.");
			$this->response->redirect('dashboard/profile');
		}

	}

}