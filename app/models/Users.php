<?php

use Phalcon\Mvc\Model;
use Phalcon\Db\RawValue;

class Users extends Model {

	public function beforeValidationOnCreate(){
		if(!$this->timestamp) {
			$this->timestamp = new RawValue('default');
		}
	}

	public $id;
	public $email;
	public $name;
	public $lastname;
	public $password;
	public $timestamp;

}