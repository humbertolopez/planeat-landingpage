<?php

use Phalcon\Mvc\Model;
use Phalcon\Db\RawValue;

class Notifications extends Model {

	public function beforeValidationOnCreate(){
		if(!$this->timestamp) {
			$this->timestamp = new RawValue('default');
		}
	}

	public $id;
	public $payment_id;
	public $timestamp;
}