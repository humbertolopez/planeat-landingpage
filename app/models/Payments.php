<?php

use Phalcon\Mvc\Model;
use Phalcon\Db\RawValue;

class Payments extends Model {

	public function beforeValidationOnCreate(){
		if(!$this->timestamp) {
			$this->timestamp = new RawValue('default');
		}
	}

	public $id;
	public $userid;
	public $preference_id;
	public $date_created;
	public $timestamp;

}