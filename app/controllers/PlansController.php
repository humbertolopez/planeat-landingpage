<?php

use Phalcon\Mvc\Controller;

class PlansController extends Controller {

	public function lilasingleAction() {
		$this->session->set(
				'_planinfo',
				[
					'_plan' => 'lilasingle',
					'_planname' => 'Plan Lila',
					'_planprice' => 199.00,
					'_type' => 'monthly'
				]
			);
		$this->flashSession->success("Crea tu cuenta PlanEat para contratar el Plan Lila por un mes.");
		$this->response->redirect('buy');
	}
	public function lilarecurringAction() {
		$this->session->set(
				'_planinfo',
				[
					'_plan' => 'lilarecurring',
					'_planname' => 'Plan Lila Mensual',
					'_planprice' => 199.00,
					'_type' => 'recurring'
				]
			);
		$this->flashSession->success("Crea tu cuenta PlanEat para suscribirte al Plan Lila.");
		$this->response->redirect('buy');	
	}
	public function labsingleAction() {
		$this->session->set(
				'_planinfo',
				[
					'_plan' => 'labsingle',
					'_planname' => 'Plan Fitness and Nutrition Lab',
					'_planprice' => 199.00,
					'_type' => 'monthly'
				]
			);
		$this->flashSession->success("Crea tu cuenta PlanEat para contratar el Plan Fitness and Nutrition Lab por un mes.");
		$this->response->redirect('buy');
	}
	public function labrecurringAction() {
		$this->session->set(
				'_planinfo',
				[
					'_plan' => 'lilarecurring',
					'_planname' => 'Plan Fitness and Nutrition Lab Mensual',
					'_planprice' => 199.00,
					'_type' => 'recurring'
				]
			);
		$this->flashSession->success("Crea tu cuenta PlanEat para suscribirte al Plan Fitness and Nutrition Lab.");
		$this->response->redirect('buy');	
	}
	public function sofiasingleAction() {
		$this->session->set(
				'_planinfo',
				[
					'_plan' => 'sofiasingle',
					'_planname' => 'Plan Sofía',
					'_planprice' => 199.00,
					'_type' => 'monthly'
				]
			);
		$this->flashSession->success("Crea tu cuenta PlanEat para contratar el Plan Sofía por un mes.");
		$this->response->redirect('buy');
	}
	public function cristinasingleAction() {
		$this->session->set(
				'_planinfo',
				[
					'_plan' => 'sofiasingle',
					'_planname' => 'Plan Cristina',
					'_planprice' => 199.00,
					'_type' => 'monthly'
				]
			);
		$this->flashSession->success("Crea tu cuenta PlanEat para contratar el Plan Cristina por un mes.");
		$this->response->redirect('buy');
	}
	public function germansingleAction() {
		$this->session->set(
				'_planinfo',
				[
					'_plan' => 'germansingle',
					'_planname' => 'Plan Germán',
					'_planprice' => 199.00,
					'_type' => 'monthly'
				]
			);
		$this->flashSession->success("Crea tu cuenta PlanEat para contratar el Plan Germán por un mes.");
		$this->response->redirect('buy');
	}
	public function dennissingleAction() {
		$this->session->set(
				'_planinfo',
				[
					'_plan' => 'dennissingle',
					'_planname' => 'Plan Dennis',
					'_planprice' => 199.00,
					'_type' => 'monthly'
				]
			);
		$this->flashSession->success("Crea tu cuenta PlanEat para contratar el Plan Dennis por un mes.");
		$this->response->redirect('buy');
	}
	public function michellesingleAction() {
		$this->session->set(
				'_planinfo',
				[
					'_plan' => 'michellesingle',
					'_planname' => 'Plan Michelle',
					'_planprice' => 199.00,
					'_type' => 'monthly'
				]
			);
		$this->flashSession->success("Crea tu cuenta PlanEat para contratar el Plan Michelle por un mes.");
		$this->response->redirect('buy');
	}
	public function planeatsingleAction() {
		$this->session->set(
				'_planinfo',
				[
					'_plan' => 'planeatsingle',
					'_planname' => 'Plan Nutricional PlanEat',
					'_planprice' => 199.00,
					'_type' => 'monthly'
				]
			);

		$userid = $this->session->get('_id');
		$userinfo = Users::findFirstById($userid);
		$payments = new Payments();

		$preference_data = [
			'items' => [
				[
					'title' => 'Plan Nutricional PlanEat',
					'quantity' => 1,
					'currency_id' => 'MXN',
					'unit_price' => 199.00,
					'external_reference' => 'planeatsingle'
				]
			],
			'payer' => [
				'email' => $userinfo->email,
				'name' => $userinfo->name,
				'surname' => $userinfo->lastname
			],
			'back_urls' => [
				'success' => $this->url->getBaseUri().'dashboard/paymentsuccess',
				'failure' => $this->url->getBaseUri().'dashboard/paymentfail',
				'pending' => $this->url->getBaseUri().'dashboard/paymentpending'
			],
			'auto_return' => 'approved'
		];

		$savepreference = $this->mp->create_preference($preference_data);

		$payments->userid = $userid;
		$payments->preference_id = $savepreference['response']['id'];
		$payments->date_created = $savepreference['response']['date_created'];

		$payments->save();

		$this->session->set('_init',$savepreference['response']['init_point']);
		$this->response->redirect('letspay');

	}

}