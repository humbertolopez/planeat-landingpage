<?php

use Phalcon\Mvc\Controller;

class IndexController extends Controller {
	public function isloggedin($userid) {
		$profile = Basicprofile::findFirstByUserid($userid);
		if(isset($userid)) {
			$this->response->redirect('dashboard');
		}
	}
	public function indexAction() {
		$this->tag->setTitle('PlanEat — Tu nuevo coach alimenticio');
		$this->view->setTemplateAfter('landing');
		$jsfiles = [
			'js/dist/owl.carousel.min.js',
			'js/magia.js'
		];
		$cssfiles = [
			'js/dist/assets/owl.carousel.min.css',
			'js/dist/assets/owl.theme.default.min.css'
		];
		$this->view->scripts = $jsfiles;
		$this->view->styles = $cssfiles;
		$userid = $this->session->get('_id');
		$this->isloggedin($userid);
	}
}