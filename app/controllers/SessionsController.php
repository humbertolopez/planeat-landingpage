<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;

class SessionsController extends Controller {

	private function _registerSession($user) {
		$this->session->set('_id',$user->id);
		$this->session->set('_name',$user->name);
		$this->session->set('_lastname',$user->lastname);
	}

	public function createuserAction() {
		$user = new Users();

		$signupemail = $this->request->getPost('signupemail');
		$signupname = $this->request->getPost('signupname');
		$signuplastname = $this->request->getPost('signuplastname');
		$pass = $this->request->getPost('signuppass');
		$repeatpass = $this->request->getPost('signuppassrepeat');

		$user->email = $signupemail;
		$user->name = $signupname;
		$user->lastname = $signuplastname;
		$user->password = $this->security->hash($pass);

		$existentuser = Users::findFirstByEmail($signupemail);

		if(!$existentuser) {
			$user->save();

			if($user->save()) {
				$this->flashSession->success("Tu nueva cuenta PlanEat ha sido creada. Ahora, usa tu e-mail y contraseña para iniciar sesión");
				$this->response->redirect('login');
			} else {
				$this->flashSession->error("¡Algo falló! Por favor, inténtalo más tarde.");
				$this->response->redirect('signup');
			}
		} else {
			$this->flashSession->error("Ya existe una cuenta registrada con este e-mail. ¿Qué tal si intentas iniciar sesión?");
			$this->response->redirect('signup');
		}

	}
	public function loginAction(){

		$email = $this->request->getPost('loginemail');
		$password = $this->request->getPost('loginpass');

		if($this->request->isPost()){

			$user = Users::findFirstByEmail($email);

			if($user){
				if($this->security->checkHash($password,$user->password)){
					$this->_registersession($user);
					//$this->flashSession->success('¡Te damos la bienvenida a PlanEat! '. $user->name);
					$this->response->redirect('dashboard');
				} else {
					$this->flashSession->error('¡Oops! Tu constraseña no coincide. Escribe tus datos nuevamente.');
					$this->response->redirect('login');	
				}
			} else {
				$this->flashSession->error('¡Oops! Este usuario no existe. ¿Ya tienes una cuenta con nosostros?');
				$this->response->redirect('login');
			}
		}
	}

	public function logoutAction(){
		$this->session->remove('_id');
		$this->session->remove('_name');
		$this->session->remove('_lastname');
		$this->session->remove('_email');
		$this->session->remove('_plan');
		$this->flashSession->error('Haz cerrado tu sesión. ¡Regresa pronto!');
		$this->response->redirect('login');
	}

}