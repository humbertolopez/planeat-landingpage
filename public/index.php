<?php

use Phalcon\Loader;
use Phalcon\Security;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Application;
use Phalcon\Mvc\Url as UrlProvider;
use Phalcon\Mvc\Router;
use Phalcon\Session\Factory;
use Phalcon\Flash\Session as FlashSession;
use Phalcon\Session\Adapter\Files as Session;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Mvc\Dispatcher;

// Define some absolute path constants to aid in locating resources
define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app/');
define('MP_PATH',BASE_PATH. '/app/mp');
// ...

$loader = new Loader();

$loader->registerDirs(
    [
        APP_PATH . '/controllers/',
        APP_PATH . '/models/',
        APP_PATH . '/config/',
    ]
);

require MP_PATH . '/mercadopago.php';

$loader->register();

$di = new FactoryDefault();

$di->set(
		'view',
		function() {
			$view = new View();
			$view->setViewsDir(APP_PATH. '/views/');
			return $view;
		}
	);
$di->set(
		'url',
		function(){
			require APP_PATH. '/config/config.php';
			$url = new UrlProvider();
			$url->setBaseUri($planeat->baseurl);
			return $url;
		}
	);

$di->set(
		'router',
		function(){
		    require APP_PATH. '/config/routes.php';
		    return $router;
		}
	);
$di->set(
		'session',
		function(){
			$session = new Session();
			$session->start();
			return $session;
		}
	);
$di->set(
	'flashSession',function()
	{
	    return new FlashSession(array(
	        'error' => 'alert alert-danger',
	        'success' => 'alert alert-success',
	        'warning' => 'alert alert-warning'
	        )
	    );
	    return $flashSession;
	}
);
$di->set(
		'db',
		function() {
			require APP_PATH. '/config/config.php';
			return new DbAdapter(
					[
						'host' => $planeat->host,
						'username' => $planeat->username,
						'password' => $planeat->password,
						'dbname' => $planeat->dbname,
						'charset' => 'utf8'
					]
				);
		}
	);
$di->set(
	'security',
	function () {
		    $security = new Security();
		    $security->setWorkFactor(12);
		    return $security;
		}, true
	);
$di->set(
	'dispatcher',
	function(){
			$dispatcher = new Dispatcher();
			return $dispatcher;
		}
	);

$di->set(
	'mp',
	function(){
			require APP_PATH. '/config/config.php';
			$mp = new MP($planeat->mp_clientid,$planeat->mp_secret);
			return $mp;
		}
	);

$application = new Application($di);

try {
    // Handle the request
    $response = $application->handle();

    $response->send();
} catch (\Exception $e) {
    echo 'Exception: ', $e->getMessage();
}