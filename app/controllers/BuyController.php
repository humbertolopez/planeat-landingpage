<?php

use Phalcon\Mvc\Controller;

class BuyController extends Controller {
	public function buypageAction() {
		$this->tag->setTitle('PlanEat — Contrata PlanEat');
	}
	public function letspaypageAction() {
		$this->tag->setTitle('PlanEat — Paga a través de MercadoPago');
	}
}