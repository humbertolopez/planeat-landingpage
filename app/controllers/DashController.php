<?php

use Phalcon\Mvc\Controller;

class DashController extends Controller {
	private function checkforpayment($userid){
		$payment = Payments::findFirstByUserid($userid);
		$collection = Collections::findFirstByUserid($userid);
		if($collection){
			return $this->view->collectiondata = $collection;
		} elseif ($payment) {
			return $this->view->paymentdata = $payment;
		}
	}

	public function isloggedin($userid) {
		$profile = Basicprofile::findFirstByUserid($userid);
		$this->view->profiledata = $profile;
		if(!isset($userid)) {
			$this->flashSession->warning("Aún no has iniciado sesión.");
			$this->response->redirect('login');
		}

	}
	public function dashboardpageAction() {
		$this->tag->setTitle('PlanEat — Dashboard');
		$userid = $this->session->get('_id');
		$this->isloggedin($userid);
		$this->checkforpayment($userid);
		$profile = Basicprofile::findFirstByUserid($userid);
		$this->view->profiledata = $profile;
		//$this->view->jsfile = 'js/dashmagia.js';
	}
	public function weeklypageAction() {
		$this->tag->setTitle('PlanEat — Tu plan alimenticio semanal');
		$this->isloggedin($userid);
	}
	public function habitspageAction() {
		$this->tag->setTitle('PlanEat — Tus hábitos alimenticios');
		$this->isloggedin($userid);
	}
	public function activitypageAction() {
		$this->tag->setTitle('PlanEat — Tu actividad física');
		$this->isloggedin($userid);
	}
	public function measurespageAction() {
		$this->tag->setTitle('PlanEat — Tus medidas y peso');
		$this->isloggedin($userid);
	}
	public function sleeppageAction() {
		$this->tag->setTitle('PlanEat — Información de sueño');
		$this->isloggedin($userid);
	}
	public function basicspageAction() {
		$this->tag->setTitle('PlanEat — Tu información básica');
		$userid = $this->session->get('_id');
		$this->isloggedin($userid);
		$profile = Basicprofile::findFirstByUserid($userid);
		$this->view->profiledata = $profile;
	}
	public function profilepageAction() {
		$this->tag->setTitle('PlanEat — Tu perfil');
		$userid = $this->session->get('_id');
		$this->isloggedin($userid);
		$user = Users::findFirstById($userid);
		$this->view->userdata = $user;
	}
	public function planpageAction() {
		$this->tag->setTitle('PlanEat — Tu plan actual');
		$userid = $this->session->get('_id');
		$this->isloggedin($userid);
	}
	public function paymentsuccesspageAction() {
		$this->tag->setTitle('PlanEat — ¡Haz pagado tu plan!');

		$userid = $this->session->get('_id');
		$this->isloggedin($userid);

		$collection = new Collections();

		$collection->userid = $userid;
		$collection->collection_id = $this->request->get('collection_id');
		$collection->collection_status = $this->request->get('collection_status');
		$collection->preference_id = $this->request->get('preference_id');
		$collection->external_reference = $this->request->get('external_reference');
		$collection->payment_type = $this->request->get('payment_type');

		$collection->save();

	}
	public function paymentfailpageAction() {
		$this->tag->setTitle('PlanEat — Tu plan actual');
		$userid = $this->session->get('_id');
		$this->isloggedin($userid);
	}

	public function paymentpendingpageAction() {
		$this->tag->setTitle('PlanEat — Tu plan actual');
		$userid = $this->session->get('_id');
		$this->isloggedin($userid);

		$collection = new Collections();

		$collection->userid = $userid;
		$collection->collection_id = $this->request->get('collection_id');
		$collection->collection_status = $this->request->get('collection_status');
		$collection->preference_id = $this->request->get('preference_id');
		$collection->external_reference = $this->request->get('external_reference');
		$collection->payment_type = $this->request->get('payment_type');

		$collection->save();
	}
}