<?php

use Phalcon\Mvc\Controller;

class PaymentsController extends Controller {

	private function _registerSession($user) {
		$this->session->set('_id',$user->id);
		$this->session->set('_name',$user->name);
		$this->session->set('_lastname',$user->lastname);
	}

	public function createandpayAction() {

		$user = new Users();
		$payments = new Payments();

		$signupemail = $this->request->getPost('signupemail');
		$signupname = $this->request->getPost('signupname');
		$signuplastname = $this->request->getPost('signuplastname');
		$pass = $this->request->getPost('signuppass');
		$repeatpass = $this->request->getPost('signuppassrepeat');

		$user->email = $signupemail;
		$user->name = $signupname;
		$user->lastname = $signuplastname;
		$user->password = $this->security->hash($pass);

		$existentuser = Users::findFirstByEmail($signupemail);

		if(!$existentuser) {

			$preference_data = [
				'items' => [
					[
						'title' => $this->session->_planinfo['_planname'],
						'quantity' => 1,
						'currency_id' => 'MXN',
						'unit_price' => $this->session->_planinfo['_planprice']
					]
				],
				'payer' => [
					'email' => $signupemail,
					'name' => $signupname,
					'surname' => $signuplastname
				],
				'back_urls' => [
					'success' => 'http://planeat.mx/',
					'failure' => 'http://planeat.mx/#fail',
					'pending' => 'http://planeat.mx/#pending'
				],
				'auto_return' => 'approved'
			];

			$user->save();

			$savepreference = $this->mp->create_preference($preference_data);

			$payments->userid = $user->id;
			$payments->preference_id = $savepreference['response']['id'];
			$payments->date_created = $savepreference['response']['date_created'];
			
			$payments->save();

			if($user->save() && $payments->save()) {
				$this->flashSession->success("Tu nueva cuenta PlanEat ha sido creada. Ahora, paga tu plan a través de MercadoPago.");
				$this->session->set('_init',$savepreference['response']['init_point']);
				$this->_registersession($user);
				$this->response->redirect('dashboard');
			} else {
				$this->flashSession->error("¡Algo falló! Por favor, inténtalo más tarde.");
				$this->response->redirect('buy');
				//$messages = $user->getMessages();
				//foreach ($messages as $message) {
	            //    echo $message->getMessage(), "<br/>";
	            //}
			}
		} else {
			$this->flashSession->error("Ya existe una cuenta registrada con este e-mail. ¿Qué tal si intentas iniciar sesión?");
			$this->response->redirect('login');
		}

	}

	public function createandpaymonthlyAction() {

		$user = new Users();

		$signupemail = $this->request->getPost('signupemail');
		$signupname = $this->request->getPost('signupname');
		$signuplastname = $this->request->getPost('signuplastname');
		$pass = $this->request->getPost('signuppass');
		$repeatpass = $this->request->getPost('signuppassrepeat');

		$user->email = $signupemail;
		$user->name = $signupname;
		$user->lastname = $signuplastname;
		$user->password = $this->security->hash($pass);

		$existentuser = Users::findFirstByEmail($signupemail);

		if(!$existentuser) {

			$preapprovaldata = [
				'payer_email' => $signupemail,
				'back_url' => 'http://localhost/planeat/dashboard',
				'reason' => $this->session->_planinfo['_planname'],
				'auto_recurring' => [
						'frequency' => 1,
						'frequency_type' => 'months',
						'transaction_amount'=> $this->session->_planinfo['_planprice'],
						'currency_id' => 'MXN'
					],

			];

			$user->save();

			$savepreapproval = $this->mp->create_preapproval_payment($preapprovaldata);
			$savepreapproval;

			if($user->save() && $savepreapproval) {
				$this->flashSession->success("Tu nueva cuenta PlanEat ha sido creada. Ahora, serás dirigido a MercadoPago.");
				$this->session->set('_init',$savepreapproval['response']['init_point']);
				$this->response->redirect('letspay');
			} else {
				$this->flashSession->error("¡Algo falló! Por favor, inténtalo más tarde.");
				$this->response->redirect('buy');
			}
		} else {
			$this->flashSession->error("Ya existe una cuenta registrada con este e-mail. ¿Qué tal si intentas iniciar sesión?");
			$this->response->redirect('buy');
		}

	}

}