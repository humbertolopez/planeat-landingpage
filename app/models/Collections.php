<?php

use Phalcon\Mvc\Model;
use Phalcon\Db\RawValue;

class Collections extends Model {

	public function beforeValidationOnCreate(){
		if(!$this->timestamp) {
			$this->timestamp = new RawValue('default');
		}
	}

	public $id;
	public $userid;
	public $collection_id;
	public $collection_status;
	public $preference_id;
	public $external_reference;
	public $payment_type;
	public $timestamp;

}